require 'socket'
require 'uri'

WEB_ROOT = './'

CONTENT_TYPE_MAPPING = {
        'html' => 'text/html',
        'txt' => 'text/plain',
        'png' => 'image/png',
        'jpg' => 'image/jpeg',
        'json' => 'application/json',
        'js' => 'application/javascript',
        'obj' => 'model',
        'zip' => 'application/zip',
        'css' => 'text/css',
        'tiff' => 'image/tiff',
        'edifact' => 'application/EDIFACT',
        'xml' => 'text/xml'
}

DEFAULT_CONTENT_TYPE = 'application/octet-stream'

def content_type(path)
  ext = File.extname(path).split(".").last
  CONTENT_TYPE_MAPPING.fetch(ext, DEFAULT_CONTENT_TYPE)
end

def requested_file(request_line)
        request_uri  = request_line.split(" ")[1]
        path         = URI.unescape(URI(request_uri).path)

        File.join(WEB_ROOT, path)
end


server = TCPServer.new("localhost",80)

loop do
    socket = server.accept
    request_line = server.gets
    
    STDERR.puts request_line
    
    path = requested_file ( request_line)
end