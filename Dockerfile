# This file is a template, and might need editing before it works on your project.
FROM ruby:2.4

RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        redis-server \
    && rm -rf /var/lib/apt/lists/*

WORKDIR /usr/src/app

RUN gem install redis

COPY . /usr/src/app

# For Sinatra
EXPOSE 80
CMD ["ruby", "./config.rb"]